﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace App1
{


    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Dictionary<int, char> charTable = new Dictionary<int, char>() {
                {10,  'A'},
                {11, 'B'},
                {12, 'C'},
                {13, 'D'},
                {14 ,'E'},
                {15, 'F'}
            };


        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            Calculate();
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            
            Calculate();
        }

        private void Calculate()
        {
            if (txtValue == null) return;

            string valueText = txtValue.Text;

            int faktorIn = int.Parse(((ComboBoxItem)cmbSystemInput.SelectedItem).Tag.ToString());
            int value = 0;
            int stelle = 0;
            for(int i = valueText.Length -1 ; i >= 0; i--)
            {
                Char chr = valueText[i];
                int currentCharVal = Convert(chr);
                if(currentCharVal > -1)
                {
                    value += (int)(currentCharVal * Math.Pow((double)faktorIn, (double)stelle));
                }
                stelle++;
            }

           int faktor = int.Parse(((ComboBoxItem)cmbSystemOutpu.SelectedItem).Tag.ToString());
            String calcValue = "";
           
               while (value > 0)
               {
                   int rest = value % faktor;
                   value = (value - rest) / faktor;

                   calcValue = GetZiffer(rest) + calcValue;
               }
           



           txtCalculatedValue.Text = calcValue;
        }

        private int Convert(char chr)
        {

            int returnVal = -1;
            if(!int.TryParse(chr.ToString(), out returnVal))
            {
                if(charTable.ContainsValue(chr))
                {
                    var possibleReturnVal = charTable.Where(c => c.Value == chr).Select(c => c.Key);
                    if (possibleReturnVal.Count() > 0)
                    {
                        returnVal = possibleReturnVal.First();
                    }
                };
            }
            return returnVal;
        }

        private string GetZiffer(int rest)
        {

            if (rest < 10)
            {
                return rest.ToString();
            }
            else
            {
                return charTable[rest].ToString();   
            }
        }

        private void txtValue_ManipulationStarting(object sender, ManipulationStartingRoutedEventArgs e)
        {
            
        }

        private void txtValue_KeyDown(object sender, KeyRoutedEventArgs e)
        {

        }

        private void txtCalculatedValue_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
